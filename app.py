#importa la libreria de flask
from flask import Flask, render_template, request, flash, session, redirect
#creamos una instacia de flask
import sqlite3
import os
from werkzeug.utils import escape
from forms.formularios import Ingresos, Salidas, Usuarios, Proveedores, Celulares, Login, g_usuario, g_prov, UsuariosF, g_usuarioF
import hashlib

app = Flask(__name__)
app.secret_key = os.urandom(24)
#decorador intrucciones propias de flsk especifica una ruta

@app.route("/", methods=["GET", "POST"])
def home():
    frm = Login()
    if frm.validate_on_submit():
        username = frm.username.data
        password = frm.password.data
        enc = hashlib.sha256(password.encode())
        pass_enc = enc.hexdigest()
        with sqlite3.connect("inventario.db") as con:
            con.row_factory = sqlite3.Row
            cursor = con.cursor()
            cursor.execute("SELECT * FROM usuario WHERE nombre = ? AND password = ?", [username, pass_enc])

            # cursor.execute(f"SELECT username FROM usuario WHERE nombre = '{username}' AND password = '{pass_enc}'")
            row = cursor.fetchone()
            if row:
                # Se crea la sesión
                session['usuario'] = username
                session['rol'] = row["rol"]
                if session["rol"] == "1":
                    return redirect("/dashboard_f")
                elif session["rol"] == "2":
                   return redirect("/dashboard_a")
                elif session["rol"] == "3":
                   return redirect("/dashboard")
            else:
                flash(message="Usuario no válido") 
                #return render_template("index.html", frm=frm)
    return render_template("index.html", frm=frm)

@app.route("/celulares", methods=["GET", "POST"])
def celulares():
    frm = Celulares()
    if 'rol' in session:
            if frm.validate_on_submit():
                if 'Buscar' in request.form:                
                    id_celulares= frm.id_celulares.data
                    with sqlite3.connect("inventario.db") as con:
                        con.row_factory = sqlite3.Row
                        cursor = con.cursor()  # Manipular la BD
                        # Prepara la sentencia SQL a ejecutar
                        cursor.execute('SELECT * FROM celulares')
                        rows = cursor.fetchall()
                        # Ejecuta la sentencia SQL
                        print("Ya se hizo conmit")
                        print(rows)
                        print(id_celulares)
                        return render_template("celulares.html", frm =frm, prov=id_celulares, inv=rows)
                elif 'min' in request.form:
                    with sqlite3.connect("inventario.db") as con:
                        con.row_factory = sqlite3.Row
                        cursor = con.cursor()  # Manipular la BD
                        # Prepara la sentencia SQL a ejecutar
                        cursor.execute('SELECT * FROM celulares')
                        rows = cursor.fetchall()
                        # Ejecuta la sentencia SQL
                        print("Ya se hizo conmit")
                        print(rows)
                        return render_template("celulares.html", frm =frm, click=1, inv=rows)
            return render_template("celulares.html", frm =frm)
    return redirect("/")
    
@app.route("/filtro_celulares")
def filtro_celulares():
    if 'rol' in session:
        with sqlite3.connect("inventario.db") as con:
            # Convierte la respueta SQL a un diccionario
            con.row_factory = sqlite3.Row
            cur = con.cursor()
            cur.execute("SELECT * FROM celulares")
            rows = cur.fetchall()
            return render_template("filtro_celulares.html", inventario=rows)
    return redirect("/")

@app.route("/dashboard")
def dashboard():
    if 'rol' in session and (session["rol"] == "3"):
        return render_template("dashboard.html")
    elif 'rol' in session and (session["rol"] == "1"):
        return render_template("dashboard_f.html")
    elif 'rol' in session and (session["rol"] == "2"):
        return render_template("dashboard_a.html")
    return redirect("/")

@app.route("/dashboard_a")
def dashboard_a():
    if 'rol' in session and (session["rol"] == "2"):
        return render_template("dashboard_a.html")    
    return redirect("/")

@app.route("/dashboard_f")
def dashboard_f():
    if 'rol' in session and (session["rol"] == "1"):
        return render_template("dashboard_f.html")    
    return redirect("/")    

@app.route("/ingresos", methods=["GET", "POST"])
def ingresos():
    if 'rol' in session and (session["rol"] == "2" or session["rol"] == "3"):
        frm = Ingresos()
        with sqlite3.connect("inventario.db") as con:
            # Convierte la respueta SQL a un diccionario
            con.row_factory = sqlite3.Row
            cur = con.cursor()
            cur.execute("SELECT * FROM proveedores")
            rows = cur.fetchall()
            # Pilla mani, primero que todo, buenas tardes :u
            # Segundo, que numero de columna es l 'NIT' dentro de tu base de datos?
            # Si es la segunda, entonces seria la '1'
            # Prueba ahi, ahora si

            # Obtener NIT de la query
            proveedores_choices = []
            for row in rows:
                proveedores_choices.append((row[1], row[1]))
            
            frm.proveedor.choices=proveedores_choices # Cuando te salia antes, que le pasabas aqui? lod
        if frm.validate_on_submit():
            if 'agregar_nueva_linea' in request.form:
                # Captura los datos del formulario
                name = frm.nombre.data
                desc = frm.descripcion.data
                stock = frm.stock.data
                min = frm.min.data
                proveedor = frm.proveedor.data
                # Conecta a la BD
                with sqlite3.connect("inventario.db") as con:
                    cursor = con.cursor()  # Manipular la BD
                    # Prepara la sentencia SQL a ejecutar
                    cursor.execute("INSERT INTO celulares (modelo, proveedor, desc, cantidad, min) VALUES(?,?,?,?,?)", [
                                name, proveedor, desc, stock, min])
                    # Ejecuta la sentencia SQL
                    con.commit()
                    return "Guardado con éxito"
            elif 'agregar_linea_existente' in request.form:
                # Captura los datos del formulario
                id_c = frm.id_producto.data
                desc = frm.descripcion.data
                stock = frm.stock.data
                # Conecta a la BD
                with sqlite3.connect("inventario.db") as con:
                    cursor = con.cursor()  # Manipular la BD
                    # Prepara la sentencia SQL a ejecutar
                    cursor.execute('SELECT * FROM celulares')
                    cursor.execute('''UPDATE celulares SET cantidad = ?, desc = ? WHERE id = ?''', [stock, desc, id_c])
                    # Ejecuta la sentencia SQL
                    con.commit()
                    return "actualizado con éxito"
        with sqlite3.connect("inventario.db") as con:
            # Convierte la respueta SQL a un diccionario
            con.row_factory = sqlite3.Row
            cur = con.cursor()
            cur.execute("SELECT * FROM celulares")
            rows = cur.fetchall()    
            return render_template("ingresos.html", frm=frm, celulares=rows)
    return redirect("/")    

@app.route("/usuarios", methods=["GET", "POST"])
def usuarios():
    frm = Usuarios()
    if 'rol' in session and (session["rol"] == "2" or session["rol"] == "3"):
        if frm.validate_on_submit():
            # Captura los datos del formulario
            rol = frm.rol.data
            username = frm.username.data
            password = frm.password.data
            enc = hashlib.sha256(password.encode())
            pass_enc = enc.hexdigest()

            # Conecta a la BD
            with sqlite3.connect("inventario.db") as con:
                cursor = con.cursor()  # Manipular la BD
                # Prepara la sentencia SQL a ejecutar
                cursor.execute("INSERT INTO usuario (nombre, rol, password) VALUES(?,?,?)", [
                            username, rol, pass_enc])
                # Ejecuta la sentencia SQL
                con.commit()
                return "Guardado con éxito"
        return render_template("usuarios.html", frm=frm)
    return redirect("/")

@app.route("/proveedores", methods=["GET", "POST"])
def proveedores():
    frm =Proveedores()
    if 'rol' in session:
        if frm.validate_on_submit():
            # Captura los datos del formulario
            NIT = frm.nit.data
            username = frm.nombre.data
            # Conecta a la BD
            with sqlite3.connect("inventario.db") as con:
                cursor = con.cursor()  # Manipular la BD
                # Prepara la sentencia SQL a ejecutar
                cursor.execute("INSERT INTO proveedores (NIT, NAME) VALUES(?,?)", [
                            NIT, username])
                # Ejecuta la sentencia SQL
                con.commit()
                return "Guardado con éxito" 
        return render_template("proveedores.html", frm=frm)
    return redirect("/")

@app.route("/logout", methods=["GET"])
def logout():
    session.clear()
    return redirect("/")

@app.route("/busqueda_de_usuarios", methods=["GET", "POST"])
def busquedadeusuarios():
    if 'rol' in session and (session["rol"] == "2" or session["rol"] == "3"):
        frm=g_usuario()
        if frm.validate_on_submit():
            if 'edit' in request.form:
                # Captura los datos del formulario
                id_g= frm.id.data
                name = frm.name.data
                rol = frm.rol.data
                password = frm.password.data
                enc = hashlib.sha256(password.encode())
                pass_enc = enc.hexdigest()
                # Conecta a la BD
                with sqlite3.connect("inventario.db") as con:
                    cursor = con.cursor()  # Manipular la BD
                    # Prepara la sentencia SQL a ejecutar
                    cursor.execute('SELECT * FROM usuario')
                    cursor.execute('''UPDATE usuario SET rol = ?, nombre = ?, password = ? WHERE id = ?''', [rol, name, pass_enc, id_g])
                    # Ejecuta la sentencia SQL
                    con.commit()
                    print("Ya se hizo conmit")
                    print(password)
                    print(pass_enc)
                    return "actualizado con éxito"
            elif 'delete' in request.form:
                # Captura los datos del formulario
                id_g= frm.id.data
                # Conecta a la BD
                with sqlite3.connect("inventario.db") as con:
                    cursor = con.cursor()  # Manipular la BD
                    # Prepara la sentencia SQL a ejecutar
                    cursor.execute('SELECT * FROM usuario')
                    cursor.execute('''DELETE FROM usuario WHERE id = ?''', [id_g])
                    # Ejecuta la sentencia SQL
                    con.commit()
                    return "Elminado con éxito"
            elif 'search' in request.form:
                # Captura los datos del formulario
                id_g= frm.id.data
                # Conecta a la BD
                with sqlite3.connect("inventario.db") as con:
                    cursor = con.cursor()  # Manipular la BD
                    # Prepara la sentencia SQL a ejecutar
                    cursor.execute('SELECT * FROM usuario WHERE id = ?', [id_g])
                    rows = cursor.fetchone()
                    # Ejecuta la sentencia SQL
                    frm.name.data=rows[1]
                    frm.rol.data=rows[2]
                    print(rows[3])
        with sqlite3.connect("inventario.db") as con:
            # Convierte la respueta SQL a un diccionario
            con.row_factory = sqlite3.Row
            cur = con.cursor()
            cur.execute("SELECT * FROM usuario")
            rows = cur.fetchall()    
            return render_template("G_usuario.html", frm=frm, usuario=rows)
    return redirect("/")

@app.route("/busqueda_proveedores", methods=["GET", "POST"])
def busquedaprov():
    if 'rol' in session:
        frm=g_prov()
        if frm.validate_on_submit():
            if 'edit' in request.form:
                # Captura los datos del formulario
                nit = frm.nit.data
                name = frm.name.data
                print(nit)
                # Conecta a la BD
                with sqlite3.connect("inventario.db") as con:
                    cursor = con.cursor()  # Manipular la BD
                    # Prepara la sentencia SQL a ejecutar
                    cursor.execute('SELECT * FROM proveedores')
                    cursor.execute('''UPDATE proveedores SET NAME = ? WHERE NIT = ?''', [name, nit])
                    # Ejecuta la sentencia SQL
                    con.commit()
                    return "actualizado con éxito"
            elif 'delete' in request.form:
                # Captura los datos del formulario
                nit = frm.nit.data
                # Conecta a la BD
                with sqlite3.connect("inventario.db") as con:
                    cursor = con.cursor()  # Manipular la BD
                    # Prepara la sentencia SQL a ejecutar
                    cursor.execute('SELECT * FROM proveedores')
                    cursor.execute('''DELETE FROM proveedores WHERE NIT = ?''', [nit])
                    # Ejecuta la sentencia SQL
                    con.commit()
                    return "Elminado con éxito"
            elif 'Crear' in request.form:
                # Captura los datos del formulario
                NIT = frm.nit.data
                username = frm.name.data
                # Conecta a la BD
                with sqlite3.connect("inventario.db") as con:
                    cursor = con.cursor()  # Manipular la BD
                    # Prepara la sentencia SQL a ejecutar
                    cursor.execute("INSERT INTO proveedores (NIT, NAME) VALUES(?,?)", [
                                NIT, username])
                    # Ejecuta la sentencia SQL
                    con.commit()
                    return "Guardado con éxito" 
        with sqlite3.connect("inventario.db") as con:
            # Convierte la respueta SQL a un diccionario
            con.row_factory = sqlite3.Row
            cur = con.cursor()
            cur.execute("SELECT * FROM proveedores")
            rows = cur.fetchall()    
            return render_template("G_prov.html", frm=frm, prov=rows)
    return redirect("/")

@app.route("/ingresos_F", methods=["GET", "POST"])
def ingresos_F():
    if 'rol' in session and (session["rol"] == "1"):
        frm = Ingresos()
        with sqlite3.connect("inventario.db") as con:
            # Convierte la respueta SQL a un diccionario
            con.row_factory = sqlite3.Row
            cur = con.cursor()
            cur.execute("SELECT * FROM proveedores")
            rows = cur.fetchall()
            # Pilla mani, primero que todo, buenas tardes :u
            # Segundo, que numero de columna es l 'NIT' dentro de tu base de datos?
            # Si es la segunda, entonces seria la '1'
            # Prueba ahi, ahora si

            # Obtener NIT de la query
            proveedores_choices = []
            for row in rows:
                proveedores_choices.append((row[1], row[1]))
            
            frm.proveedor.choices=proveedores_choices # Cuando te salia antes, que le pasabas aqui? lod
        if frm.validate_on_submit():
            if 'agregar_nueva_linea' in request.form:
                # Captura los datos del formulario
                name = frm.nombre.data
                desc = frm.descripcion.data
                stock = frm.stock.data
                min = frm.min.data
                proveedor = frm.proveedor.data
                # Conecta a la BD
                with sqlite3.connect("inventario.db") as con:
                    cursor = con.cursor()  # Manipular la BD
                    # Prepara la sentencia SQL a ejecutar
                    cursor.execute("INSERT INTO celulares (modelo, proveedor, desc, cantidad, min) VALUES(?,?,?,?,?)", [
                                name, proveedor, desc, stock, min])
                    # Ejecuta la sentencia SQL
                    con.commit()
                    return "Guardado con éxito"
            elif 'agregar_linea_existente' in request.form:
                # Captura los datos del formulario
                id_c = frm.id_producto.data
                desc = frm.descripcion.data
                stock = frm.stock.data
                # Conecta a la BD
                with sqlite3.connect("inventario.db") as con:
                    cursor = con.cursor()  # Manipular la BD
                    # Prepara la sentencia SQL a ejecutar
                    cursor.execute('SELECT * FROM celulares')
                    cursor.execute('''UPDATE celulares SET cantidad = ?, desc = ? WHERE id = ?''', [stock, desc, id_c])
                    # Ejecuta la sentencia SQL
                    con.commit()
                    return "actualizado con éxito"
        with sqlite3.connect("inventario.db") as con:
            # Convierte la respueta SQL a un diccionario
            con.row_factory = sqlite3.Row
            cur = con.cursor()
            cur.execute("SELECT * FROM celulares")
            rows = cur.fetchall()    
            return render_template("ingresos_F.html", frm=frm, celulares=rows)
    return redirect("/")    

@app.route("/busqueda_de_usuariosF", methods=["GET", "POST"])
def busquedadeusuarios_F():
    if 'rol' in session and (session["rol"] == "1"):
        frm=g_usuario()
        if frm.validate_on_submit():
            if 'edit' in request.form:
                # Captura los datos del formulario
                id_g= frm.id.data
                name = frm.name.data
                rol = frm.rol.data
                password = frm.password.data
                enc = hashlib.sha256(password.encode())
                pass_enc = enc.hexdigest()
                # Conecta a la BD
                with sqlite3.connect("inventario.db") as con:
                    cursor = con.cursor()  # Manipular la BD
                    # Prepara la sentencia SQL a ejecutar
                    cursor.execute('SELECT * FROM usuario')
                    cursor.execute('''UPDATE usuario SET rol = ?, nombre = ?, password = ? WHERE id = ?''', [rol, name, pass_enc, id_g])
                    # Ejecuta la sentencia SQL
                    con.commit()
                    print("Ya se hizo conmit")
                    print(password)
                    print(pass_enc)
                    return "actualizado con éxito"
            elif 'delete' in request.form:
                # Captura los datos del formulario
                id_g= frm.id.data
                # Conecta a la BD
                with sqlite3.connect("inventario.db") as con:
                    cursor = con.cursor()  # Manipular la BD
                    # Prepara la sentencia SQL a ejecutar
                    cursor.execute('SELECT * FROM usuario')
                    cursor.execute('''DELETE FROM usuario WHERE id = ?''', [id_g])
                    # Ejecuta la sentencia SQL
                    con.commit()
                    return "Elminado con éxito"
            elif 'search' in request.form:
                # Captura los datos del formulario
                id_g= frm.id.data
                # Conecta a la BD
                with sqlite3.connect("inventario.db") as con:
                    cursor = con.cursor()  # Manipular la BD
                    # Prepara la sentencia SQL a ejecutar
                    cursor.execute('SELECT * FROM usuario WHERE id = ?', [id_g])
                    rows = cursor.fetchone()
                    # Ejecuta la sentencia SQL
                    frm.name.data=rows[1]
                    frm.rol.data=rows[2]
                    print(rows[3])
        with sqlite3.connect("inventario.db") as con:
            # Convierte la respueta SQL a un diccionario
            con.row_factory = sqlite3.Row
            cur = con.cursor()
            cur.execute("SELECT * FROM usuario")
            rows = cur.fetchall()    
            return render_template("G_usuario.html", frm=frm, usuario=rows)
    return redirect("/")

@app.route("/usuariosF", methods=["GET", "POST"])
def usuariosF():
    frm = UsuariosF()
    if 'rol' in session and (session["rol"] == "2" or session["rol"] == "3"):
        if frm.validate_on_submit():
            # Captura los datos del formulario
            rol = frm.rol.data
            username = frm.username.data
            password = frm.password.data
            enc = hashlib.sha256(password.encode())
            pass_enc = enc.hexdigest()

            # Conecta a la BD
            with sqlite3.connect("inventario.db") as con:
                cursor = con.cursor()  # Manipular la BD
                # Prepara la sentencia SQL a ejecutar
                cursor.execute("INSERT INTO usuario (nombre, rol, password) VALUES(?,?,?)", [
                            username, rol, pass_enc])
                # Ejecuta la sentencia SQL
                con.commit()
                return "Guardado con éxito"
        return render_template("usuariosF.html", frm=frm)
    return redirect("/")

@app.route("/busqueda_de_usuariosA", methods=["GET", "POST"])
def busquedadeusuariosF():
    if 'rol' in session and (session["rol"] == "2"):
        frm=g_usuarioF()
        if frm.validate_on_submit():
            if 'edit' in request.form:
                # Captura los datos del formulario
                id_g= frm.id.data
                name = frm.name.data
                rol = frm.rol.data
                password = frm.password.data
                enc = hashlib.sha256(password.encode())
                pass_enc = enc.hexdigest()
                # Conecta a la BD
                with sqlite3.connect("inventario.db") as con:
                    cursor = con.cursor()  # Manipular la BD
                    # Prepara la sentencia SQL a ejecutar
                    cursor.execute('SELECT * FROM usuario')
                    cursor.execute('''UPDATE usuario SET rol = ?, nombre = ?, password = ? WHERE id = ?''', [rol, name, pass_enc, id_g])
                    # Ejecuta la sentencia SQL
                    con.commit()
                    print("Ya se hizo conmit")
                    print(password)
                    print(pass_enc)
                    return "actualizado con éxito"
            elif 'delete' in request.form:
                # Captura los datos del formulario
                id_g= frm.id.data
                # Conecta a la BD
                with sqlite3.connect("inventario.db") as con:
                    cursor = con.cursor()  # Manipular la BD
                    # Prepara la sentencia SQL a ejecutar
                    cursor.execute('SELECT * FROM usuario')
                    cursor.execute('''DELETE FROM usuario WHERE id = ?''', [id_g])
                    # Ejecuta la sentencia SQL
                    con.commit()
                    return "Elminado con éxito"
            elif 'search' in request.form:
                # Captura los datos del formulario
                id_g= frm.id.data
                # Conecta a la BD
                with sqlite3.connect("inventario.db") as con:
                    cursor = con.cursor()  # Manipular la BD
                    # Prepara la sentencia SQL a ejecutar
                    cursor.execute('SELECT * FROM usuario WHERE id = ?', [id_g])
                    rows = cursor.fetchone()
                    # Ejecuta la sentencia SQL
                    frm.name.data=rows[1]
                    frm.rol.data=rows[2]
                    print(rows[3])
        with sqlite3.connect("inventario.db") as con:
            # Convierte la respueta SQL a un diccionario
            con.row_factory = sqlite3.Row
            cur = con.cursor()
            cur.execute("SELECT * FROM usuario")
            rows = cur.fetchall()    
            return render_template("G_usuarioF.html", frm=frm, usuario=rows)
    return redirect("/")
app.run(debug=True)